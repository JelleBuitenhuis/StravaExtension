# Strava Ranks Chrome Extension

Because Strava removed some options from segments, but still sends the data in their API requests, I've decided to show that data again. 

### Features:
  * You can now sort the table containing your segments, just click on the relevant table header
  * On each segment you can see your personal rank, the total amount of attempts, and the percentile of your best attempt
  * You can see all attempts you made on a segment
  * The analyse button on the activity page now works
  * The analysis button on a running segment takes you to the analysis page with the segment selected
  * The analyse button on a bike segment now brings you to the analysis page with the segment selected
  * Because of the changes to manifest v3, it is no longer possible to modify web requests, the following feature has been removed:
    * ~~Compare any two activities on the flyby screen. Use the following pattern: https:\/\/labs\.strava\.com\/flyby\/viewer\/#`\d+(\/\d+)+`~~
      * ~~If you add `?equalize=true` all flybys will have the same starting time~~
      * ~~E.g. `https://labs.strava.com/flyby/viewer/#4267063442/4266960806/4267455658?equalize=true`~~
        * ~~`4267063442` is the main activity id to compare against~~
        *  ~~`4266960806` and `4267455658` are the activities to be compared against. This list can be infinitely big~~
        * ~~`?equalize=true` equalizes all starting timesv
  * ~~A screen to mass add flybys for custom comparison~~
### Installation:
This extension has been published in the [Chrome Web Store](https://chrome.google.com/webstore/detail/strava-ranks/mpmdahfbijkdjemjnglicojlobgknckb).
1. Grab and extract the zipfile [StravaExtension.zip](https://gitlab.com/JelleBuitenhuis/StravaExtension/-/jobs/artifacts/master/raw/StravaExtension.zip?job=zip)
2. Open [chrome://extensions](chrome://extensions)
3. In the topright click on "Developer mode"
4. Click on "Load unpacked"
5. Point to the unpacked zipfile
### Firefox:
This extension is also available for Firefox:
1. [https://addons.mozilla.org/en-US/firefox/addon/strava-ranks/](https://addons.mozilla.org/en-US/firefox/addon/strava-ranks/)
2. [StravaExtension-Firefox.zip](https://gitlab.com/JelleBuitenhuis/StravaExtension-Firefox/-/jobs/artifacts/main/raw/StravaExtension.zip?job=zip)
